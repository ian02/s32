
//use directive keyword require to make a module useful in the current file/module
const express = require ('express'); 

//express function is our server stored in a constant variable app
const app = express();

//require mongoose module to be used in our entry point file index.js
const mongoose = require ('mongoose');

const PORT = 3001;

//middlewares
app.use (express.json());
app.use (express.urlencoded ({extended:true}));

//Connecting userRoutes module to index.js entry point
const userRoutes = require ('./routes/userRoutes')


mongoose.connect ('mongodb+srv://admin:admin1234@zuitt-bootcamp.ktm2j.mongodb.net/courseBooking?retryWrites=true&w=majority', 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log ('Connected to Database'));


//middleware entry point url (root url before any endpoints)
app.use('/api/users', userRoutes )

//Server listening to port 3001
app.listen(PORT, () => console.log (`Server is running at ${PORT}`));

