const express = require ('express');

//Router() handles the requests
const router = express.Router();


//syntax: router.HTTP method ('uri', <request listener>)
router.get ('/', (req, res) => {
	//console.log ('Hello from userRoutes')
	res.send ('Hello from userRoutes!')
})

module.exports = router;